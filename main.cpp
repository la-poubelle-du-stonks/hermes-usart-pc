/**
 * Bridges a virtual HermesCAN bus with a real HermesCAN bus using Hermes-plus
*/

#include <iostream>
#include <semaphore>
#include <stdint.h>
#include <thread>
#include <queue>
#include <termios.h>
#include <fcntl.h>      // File control definitions

#include "hermes-cpp/socketcan.hpp"
#include "hermes/hermes-plus.hpp"

class Serial { // https://stackoverflow.com/a/18134892

private:
    bool opened = false;
    int serialPort;

    struct termios tty;

public:
    Serial (const char *port) { 
        
        serialPort = open(port, O_RDWR | O_NOCTTY);

        memset(&tty, 0, sizeof(tty));

        if (tcgetattr(serialPort, &tty) != 0) {
            std::cout << "Failed to get tty infos about " << port 
                      << ": " << strerror(errno) << std::endl;
            return;
        }

        // Baud rate 1000000
        cfsetospeed(&tty, (speed_t)B1000000);
        cfsetispeed(&tty, (speed_t)B1000000);

        // 8n1, no flow, no parity
        tty.c_cflag     &=  ~PARENB;            // Make 8n1
        tty.c_cflag     &=  ~CSTOPB;
        tty.c_cflag     &=  ~CSIZE;
        tty.c_cflag     |=  CS8;

        tty.c_cflag     &=  ~CRTSCTS;           // no flow control
        tty.c_cc[VMIN]   =  1;                  // read doesn't block
        tty.c_cc[VTIME]  =  5;                  // 0.5 seconds read timeout
        tty.c_cflag     |=  CREAD | CLOCAL;     // turn on READ & ignore ctrl lines

        // Raw
        cfmakeraw(&tty);

        // Flush and configure
        tcflush(serialPort, TCIFLUSH);
        if (tcsetattr(serialPort, TCSANOW, &tty) != 0) {
            std::cout << "Failed to configure tty " << port 
                      << ": " << errno << std::endl;
            return;
        }

        opened = true;
    }

    void send (std::string& payload) {

        if (!opened) return;

        auto c_str = payload.c_str();
        auto len = payload.size();

        write(serialPort, c_str, len);

        char ret = '\r';
        write(serialPort, &ret, 1);

    }

    std::string get () {

        if (!opened) return "";

        std::string buffer = "";
        char c = 0;

        while (c != '\n') {
            read(serialPort, &c, 1);
            buffer += c;
        }

        return buffer.substr(0, buffer.size() - 2);

    }

    bool isOpen () {
        return opened;
    }

};

class HermesUSART {

private:
    const char *can, *COM;
    std::thread CANThread, COMThread, WriteThread;

    bool run = false;

    Core::Sockets::SocketCAN canSock;
    Serial comSock;

    std::binary_semaphore canQueueSem {1};
    std::queue<Hermes::CANBuffer*> canQueue;
    
    std::binary_semaphore comQueueSem {1};
    std::queue<std::string> comQueue;

    std::binary_semaphore writeSem {0};

public:
    HermesUSART(const char *can, const char *COM): can(can), COM(COM), comSock(COM) {

        // Opening ports
        if (canSock.open((char*) can) != Core::Sockets::SOCK_OK) {
            std::cout << "Cannot open CAN interface " << can << std::endl;
            return;
        }

        if (!comSock.isOpen()) {
            std::cout << "Cannot open COM interface " << COM << std::endl;
            return;
        }

        // Starting threads
        run = true;
        CANThread = std::thread(&HermesUSART::CANReaderThread, this);
        COMThread = std::thread(&HermesUSART::COMReaderThread, this);
        WriteThread = std::thread(&HermesUSART::BothWriterThread, this);

    }

    ~HermesUSART () {

        if (!run) {
            return;
        }

        run = false;

        if (CANThread.joinable()) {
            pthread_cancel(CANThread.native_handle());
            CANThread.join();
        }

        if (COMThread.joinable()) {
            pthread_cancel(COMThread.native_handle());
            COMThread.join();
        }

        flush();
        if (WriteThread.joinable()) {
            pthread_cancel(WriteThread.native_handle());
            WriteThread.join();
        }

    }

    void start () {

        if (!run) return;

        std::string buf;
        while (!std::cin.eof()) {
            std::cin >> buf;
            comSock.send(buf);
        }

    }

    void flush () {
        writeSem.release();
    }

    bool writeCAN (std::string &base64) {
        
        auto buf = new Hermes::CANBuffer;

        if (!Hermes::parseBuffer(base64, buf)) {
            delete buf;
            return false;
        }

        canQueueSem.acquire();
        canQueue.push(buf);
        canQueueSem.release();

        flush();

        return true;

    }

    void writeCOM (Hermes::CANBuffer& buffer) {

        auto str = Hermes::serializeBuffer(&buffer);

        comQueueSem.acquire();
        comQueue.push(str);
        comQueueSem.release();

        flush();

    }

    std::vector<std::string> parseArgs (std::string &signal) {

        std::vector<std::string> command;
        std::string buf = "";
        
        for (auto &c: signal) {
            if (c == '@') continue;
            if (c == ':') {
                command.push_back(buf);
                buf = "";
                continue;
            }
            buf += c;
        }
        command.push_back(buf);

        return command;

    }

    void processSignal (std::string &signal) {

        auto command = parseArgs(signal);

        auto &cmd = command[0];

        if (cmd == "print") {
            std::cout << command[1] << std::endl;
        }

        else if (cmd == "err") {

            auto &err = command[1];

            std::cout << "Erreur STM: ";

            if (err == "can_setup_err") {
                std::cout << "Initialisation du CAN impossible";
            } 
            else if (err == "can_it_setup_err") {
                std::cout << "Initialisation des interruptions CAN impossible";
            } 
            else if (err == "can_err_flag") {
                std::cout << "HAL CAN n°" << command[2];
            } 
            else if (err == "invalid_buffer") {
                std::cout << "Buffer base64 invalide : " << command[2];
            }
            else if (err == "can_rx_fifo") {
                std::cout << "Fifo " << command[2] << " illisible";
            }
            else if (err == "can_tx") {
                std::cout << "Impossible d'utiliser la mailbox " << command[2];
            }
            else {
                std::cout << err;
            }

            std::cout << std::endl;

        }

        else if (cmd == "ok") {

            std::cout << "Validation : " << command[1] << std::endl;

        }

    }

    void CANReaderThread () {

        uint32_t id;
        Hermes::CANBuffer buf;

        std::cout << "Starting CAN Reader" << std::endl;

        while (run) {
            
            auto code = canSock.readData(&id, buf.data, (uint32_t*) &buf.len);
            
            if (code) {
                printf("CAN READ ERROR: %d\n\r", code);
                continue;
            }

            Hermes_parseCANFrameID(
                id, &buf.destination, &buf.command, &buf.sender, &buf.isResponse
            );

            writeCOM(buf);

        }
        std::cout << "Stopping CAN Reader" << std::endl;

    }

    void COMReaderThread () {

        std::cout << "Starting COM Reader" << std::endl;

        while (run) {

            auto payload = comSock.get();
            
            if (payload[0] == '@') {
                processSignal(payload);
                continue;
            }

            if (!writeCAN(payload)) {
                std::cout << "Invalid buffer: " << payload << std::endl;
            }
            
        }

        std::cout << "Stopping COM Reader" << std::endl;

    }

    void BothWriterThread () {

        std::cout << "Starting Writer Thread" << std::endl;

        int toWrite;

        while (run) {

            writeSem.acquire();

            canQueueSem.acquire();
            for (toWrite = canQueue.size(); toWrite; toWrite--) {
                auto frame = canQueue.front();

                auto id = Hermes_serializeCANFrameID(
                    frame->destination, frame->command, 
                    frame->sender, frame->isResponse
                );
                canSock.writeData(id, frame->data, frame->len);
                
                delete frame;
                canQueue.pop();
            }
            canQueueSem.release();

            comQueueSem.acquire();
            for (toWrite = comQueue.size(); toWrite; toWrite--) {
                auto &str = comQueue.front();
                comSock.send(str);
                comQueue.pop();
            }
            comQueueSem.release();

        }

        std::cout << "Stopping Writer Thread" << std::endl;

    }

};

int main (int argc, char **argv) {

    if (argc < 3) {
        printf(
            "Usage: %s <can_interface> <com_port>\n\rUse 'ip a' to find one\n\r",
            argv[0]
        );
        return 1;
    }

    HermesUSART hermes(argv[1], argv[2]);

    hermes.start();

    // MAAk+HABAM3b3AAAAAAA
    // cansend vcan0 120019fc#736f3700

    return 0;

}